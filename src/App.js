import React, { useState } from 'react';
import { BrowserRouter, Route, Link, Routes } from 'react-router-dom'; // Promijenjeno

import ContractsList from './ContractsList';
import AddContractForm from './AddContractForm';
import './App.css';

export default function App() {
  const [contracts, setContracts] = useState([]);

  const handleAddContract = (contractData) => {
    setContracts([...contracts, contractData]);
  };

  return (
    <BrowserRouter>
      <div className="App">
        <nav className='nav'>
          <ul>
            <li>
              <Link to="/">All contracts</Link>
            </li>
            <li>
              <Link to="/add-contract">Add new contract</Link>
            </li>
          </ul>
        </nav>

        <Routes> {/* Promijenjeno */}
          <Route path="/add-contract" element={<AddContractForm onAddContract={handleAddContract} />} /> {/* Promijenjeno */}
          <Route path="/" element={<ContractsList contracts={contracts} />} /> {/* Promijenjeno */}
        </Routes> {/* Promijenjeno */}
      </div>
    </BrowserRouter>
  );
}

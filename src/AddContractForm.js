import React, { useState } from 'react';
import './App.css';
import { initializeApp } from 'firebase/app';
import { getFirestore, collection, addDoc} from 'firebase/firestore';
const firebaseConfig = {
  apiKey: "AIzaSyBA2TmwkVWq6SheUkwmOK9pTcFFIMeVyos",
  authDomain: "web-app-3a108.firebaseapp.com",
  projectId: "web-app-3a108",
  storageBucket: "web-app-3a108.appspot.com",
  messagingSenderId: "267860104062",
  appId: "1:267860104062:web:6706e61de46891fc74d92f",
  measurementId: "G-391MR4LJ86"
};
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

function AddContractForm({ onAddContract }) {
  const [contractData, setContractData] = useState({
    contractName: '',
    customer: '',
    startDate: '',
    duration: '',
    comments: '',
    contractValue: '',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setContractData({ ...contractData, [name]: value });
  };

  const handleAddContract = async() => {
    if(contractData.contractName &&
      contractData.customer &&
      contractData.startDate &&
      contractData.duration &&
      contractData.contractValue
    ){
      const contractsRef = collection(db,'contracts');
      try{
        await addDoc(contractsRef, contractData);
        onAddContract(contractData);
        const storedContracts = JSON.parse(localStorage.getItem('contracts')) || [];
        storedContracts.push(contractData);
        localStorage.setItem('contracts',JSON.stringify(storedContracts));
        setContractData({
          contractName: '',
          customer: '',
          startDate: '',
          duration: '',
          comments: '',
          contractValue: '',
        });
      } catch(error) {
        console.error('Error adding document: ',error);
      }
    } else{
      alert("Please fill all required fields");
    }
  };

  return (
    <div className="contract-form">
      <h2>Add new contract</h2>
      <form>
        <div className="form-group">
          <label>Contract Name (reference):</label>
          <div className='input-with-icon'>
          <input
            type="text"
            name="contractName"
            value={contractData.contractName}
            onChange={handleInputChange}
            required
          />
          <span className='required'>*</span>
          </div>
        </div>
        <div className="form-group">
          <label>Client Name:</label>
          <div className='input-with-icon'>
          <input
            type="text"
            name="customer"
            value={contractData.customer}
            onChange={handleInputChange}
            required
          />
          <span className='required'>*</span>
          </div>
        </div>
        <div className="form-group">
          <label>Start Date of Contract:</label>
          <div className='input-with-icon'>
          <input
            type="date"
            name="startDate"
            value={contractData.startDate}
            onChange={handleInputChange}
            required
          />
          <span className='required'>*</span>
          </div>
        </div>
        <div className="form-group">
          <label>Contract Duration (months):</label>
          <div className='input-with-icon'>
          <input
            type="text"
            name="duration"
            value={contractData.duration}
            onChange={handleInputChange}
            required
          />
          <span className='required'>*</span>
          </div>
        </div>
        <div className="form-group">
          <label>Comments:</label>
          <textarea
            name="comments"
            value={contractData.comments}
            onChange={handleInputChange}
          />
        </div>
        <div className="form-group">
          <label>Contract Value ($):</label>
          <div className='input-with-icon'>
          <input
            type="text"
            name="contractValue"
            value={contractData.contractValue}
            onChange={handleInputChange}
            required
          />
          <span className='required'>*</span>
          </div>
          <div className='required-description'>'*'- This field is required to fill</div>
        </div>
        <button type="button" onClick={handleAddContract}>
          Save Contract
        </button>
      </form>
    </div>
  );
}

export default AddContractForm;

import React, { useEffect, useState } from 'react';
import './App.css';

function ContractsList({ contracts }) {
  const [filter, setFilter] = useState('');
  const [filteredContracts, setFilteredContracts] = useState([]);
  const [originalContracts, setOriginalContracts] = useState([]);
  const [sortOrder, setSortOrder] = useState('asc');
  const [sortColumn, setSortColumn] = useState(null);

  useEffect(() => {
    const storedContracts = JSON.parse(localStorage.getItem('contracts'));
    const storedSortOrder = localStorage.getItem('sortOrder') || 'asc';
    const storedSortColumn = localStorage.getItem('sortColumn') || null;
    if (storedContracts) {
      setOriginalContracts(storedContracts);
      setFilteredContracts(storedContracts);
    } else {
      setOriginalContracts(contracts);
      setFilteredContracts(contracts);
    }
    setSortOrder(storedSortOrder);
    setSortColumn(storedSortColumn);
  }, [contracts]);

  const handleFilterChange = (e) => {
    const filterValue = e.target.value;
    setFilter(filterValue);
    filterContracts(filterValue);
  };

  const filterContracts = (filterValue) => {
    const filterLowerCase = filterValue.toLowerCase();
    if (filterValue === '') {
      setFilteredContracts(originalContracts);
    } else {
      const filtered = originalContracts.filter((contract) => {
        const customerLowerCase = contract.customer.toLowerCase();
        return customerLowerCase.startsWith(filterLowerCase);
      });
      setFilteredContracts(filtered);
    }
  };

  const handleSortByDate = () => {
    const newOrder = sortOrder === 'asc' ? 'desc' : 'asc';
    setSortOrder(newOrder);
    setSortColumn('startDate');
    sortContractsByDate();
    saveSortStateToLocalStorage('startDate',newOrder);
  };

  const handleSortByColumn = (column) => {
    const newOrder = sortOrder === 'asc' ? 'desc' : 'asc';
    setSortOrder(newOrder);
    setSortColumn(column);
    sortContractsByColumn(column);
    saveSortStateToLocalStorage(column,newOrder);
  };

  const saveSortStateToLocalStorage = (column,order) => {
    localStorage.setItem('sortColumn',column);
    localStorage.setItem('sortOrder',order);
  }

  const getSortIndicator = (order) => (order === 'asc' ? '▲' : '▼');

  const sortContractsByDate = () => {
    const sorted = [...filteredContracts].sort((a,b) => {
      const dateA = new Date(a.startDate);
      const dateB = new Date(b.startDate);
      return sortColumn === 'startDate' ? (sortOrder === 'asc' ? dateA-dateB : dateB - dateA) : 0;
    });
    setFilteredContracts(sorted);
  }

  const sortContractsByColumn = (column) => {
    const sorted = [...filteredContracts].sort((a,b) => {
      if(column === 'startDate'){
        const dateA = new Date(a.startDate);
        const dateB = new Date(b.startDate);
        return sortOrder === 'asc' ? dateA - dateB : dateB - dateA;
      }
      return 0;
    });
    setFilteredContracts(sorted);
  }

  const handleDeleteContract = (index) => {
    const updatedContracts = [...filteredContracts];
    const deletedContract = updatedContracts.splice(index, 1)[0];
    setFilteredContracts(updatedContracts);
    const storedContracts = JSON.parse(localStorage.getItem('contracts')) || [];
    const updatedStoredContracts = storedContracts.filter(
      (contract) => contract.contractName !== deletedContract.contractName
    );
    localStorage.setItem('contracts', JSON.stringify(updatedStoredContracts));
  };

  const handleEditContract = (index) => {
    const updatedContracts = [...filteredContracts];
    updatedContracts[index].editable = true;
    setFilteredContracts(updatedContracts);
  };

  const handleSaveContract = (index) => {
    const updatedContracts = [...filteredContracts];
    updatedContracts[index].editable = false;
    setFilteredContracts(updatedContracts);
    localStorage.setItem('contracts',JSON.stringify(updatedContracts));
  };

  const handleInputChange = (e, index) => {
    const {name, value} = e.target;
    const updatedContracts = [...filteredContracts];
    updatedContracts[index][name] = value;
    setFilteredContracts(updatedContracts);
  };

  return (
    <div className="contracts-list">
      <h2 className='list-header'>All Contracts</h2>
      <div className='filter-sort'>
        <label>Filter by Client Name: </label>
        <input type='text' value={filter} onChange={handleFilterChange} />
        <span className='sort-indicator' onClick={handleSortByDate}>
        </span>
      </div>
      <table className='contracts-table'>
        <thead>
          <tr>
            <th>Contract Name</th>
            <th>Client Name</th>
            <th onClick={handleSortByDate} style={{ cursor:'pointer'}}>
              Start Date of Contract
              {sortColumn === 'startDate' && getSortIndicator(sortOrder)}
            </th>
            <th>Contract Duration</th>
            <th>Comments</th>
            <th>Contract Value</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {filteredContracts.map((contract, index) => (
            <tr key={index}>
              <td>
                {contract.editable ?(
                  <input
                    type='text'
                    name='contractName'
                    value={contract.contractName}
                    onChange={(e) => handleInputChange(e, index)}
                  />
                ):(
                  contract.contractName
                )}
              </td>
              <td>
                {contract.editable ?(
                  <input
                    type='text'
                    name='customer'
                    value={contract.customer}
                    onChange={(e) => handleInputChange(e, index)}
                  />
                ):(
                  contract.customer
                )}
              </td>
              <td>
                {contract.editable ?(
                  <input
                    type='date'
                    name='startDate'
                    value={contract.startDate}
                    onChange={(e) => handleInputChange(e, index)}
                  />
                ):(
                  contract.startDate
                )}
              </td>
              <td>
                {contract.editable ?(
                  <input
                    type='text'
                    name='duration'
                    value={contract.duration}
                    onChange={(e) => handleInputChange(e, index)}
                  />
                ):(
                  contract.duration
                )}
              </td>
              <td>
                {contract.editable ?(
                  <input
                    name='comments'
                    value={contract.comments}
                    onChange={(e) => handleInputChange(e, index)}
                  />
                ):(
                  contract.comments
                )}
              </td>
              <td>
                {contract.editable ?(
                  <input
                    type='text'
                    name='contractValue'
                    value={contract.contractValue}
                    onChange={(e) => handleInputChange(e, index)}
                  />
                ):(
                  contract.contractValue
                )}
              </td>
              <td>
                {contract.editable ?(
                  <button onClick={() => handleSaveContract(index)} className='save-button'>Save</button>
                ):(
                  <div className='edit-delete-container'>
                    <button onClick={() => handleEditContract(index)} className='edit-button'>Edit</button>
                    <button onClick={() => handleDeleteContract(index)} className='delete-button'>Delete</button>
                  </div>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ContractsList;
